﻿using System.Collections.Generic;
using System.Linq;
using ExpensesManager.BusinessLogic.Criteria;
using ExpensesManager.BusinessLogic.Implementation;
using ExpensesManager.DataAccess.DataModel;
using ExpensesManager.DataAccess.Interfaces;
using Mindscape.LightSpeed.Querying;
using Moq;
using Xunit;

namespace ExpensesManager.BusinessLogic.Tests
{
    public class CategoryServiceTests
    {
        [Fact]
        public void FindProducts_Test()
        {
            var mock = new Mock<ICategoryRepository>();
            mock.Setup(repository => repository.FindBy(It.IsAny<QueryExpression>()))
                .Returns(new List<Category> { new Category { Id = 1, CategoryName = "TestCategory" } });

            var service = new CategoryService(mock.Object, null, null);

            var result = service.FindCategories(new CategoryCriteria { EntityId = 1 });
            Assert.Equal("TestCategory", result.First().CategoryName);
        }
    }
}

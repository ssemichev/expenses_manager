﻿using System;
using System.Linq;
using ExpensesManager.DataAccess.DataModel;
using ExpensesManager.DataAccess.Implementation;
using Mindscape.LightSpeed;
using Mindscape.LightSpeed.Audit;
using Mindscape.LightSpeed.Linq;
using Xunit;

namespace ExpensesManager.DataAccess.Tests
{
    public class ExpenseRepositoryTests
    {
        private static readonly LightSpeedContext<ExpensesManagerDataModelUnitOfWork> Context =
            new LightSpeedContext<ExpensesManagerDataModelUnitOfWork>("default")
            {
                AuditInfoMode = AuditInfoMode.WindowsIdentity,
                AutoTimestampMode = AutoTimestampMode.Local,
                QuoteIdentifiers = true
            };

        [Fact]
        public void Get_All_Expenses_Test()
        {
            using (var repository = new ExpenseRepository(Context))
            {
                var expenses = repository.GetAll().ToList();
                Assert.True(expenses.Count >= 0);
            }
        }

        [Fact]
        public void Get_All_ExpensesWithDetails_Test()
        {
            using (var repository = new ExpenseRepository(Context))
            {
                var entities = repository.UnitOfWork.Expenses
                                    .WithAggregate(ExpensesManagerDataModelAggregates.WithExpenseDetails)
                                    .OrderByDescending(c => c.CreatedOn)
                                    .ToList();
                Assert.NotNull(entities);
            }
        }

        [Fact]
        public void Add_Expense_Test()
        {
            Category category;
            SubCategory subCategory;

            using (var repository = new CategoryRepository(Context))
            {
                category = repository.UnitOfWork.Categories
                    .WithAggregate(ExpensesManagerDataModelAggregates.WithExpenseDetails)
                    .OrderByDescending(c => c.CreatedOn).First();
                Assert.NotNull(category);
                Assert.NotNull(category.SubCategories);
                Assert.NotNull(category.SubCategories.First());
                subCategory = category.SubCategories.First();
            }

            using (var repository = new ExpenseRepository(Context))
            {
                var expense = repository.CreateEntity(new Expense
                                                           {
                                                               CategoryId = category.Id,
                                                               SubCategoryId = subCategory.Id,
                                                               Description = "Unit test expense",
                                                               Amount = new decimal(12.5)
                                                           });
                repository.Save();
                Assert.True(expense.CreatedOn >  DateTime.Now.AddMinutes(-5));
                Assert.Equal(category.CategoryName, expense.CategoryName);
                Assert.Equal(subCategory.CategoryName, expense.SubCategoryName);
            }
        }
    }
}

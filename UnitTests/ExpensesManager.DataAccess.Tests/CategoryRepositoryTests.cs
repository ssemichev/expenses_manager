﻿using System;
using System.Linq;
using ExpensesManager.DataAccess.DataModel;
using ExpensesManager.DataAccess.Implementation;
using Mindscape.LightSpeed;
using Mindscape.LightSpeed.Audit;
using Mindscape.LightSpeed.Linq;
using Xunit;

namespace ExpensesManager.DataAccess.Tests
{
    public class CategoryRepositoryTests
    {
        private static readonly LightSpeedContext<ExpensesManagerDataModelUnitOfWork> Context =
            new LightSpeedContext<ExpensesManagerDataModelUnitOfWork>("default")
            {
                AuditInfoMode = AuditInfoMode.WindowsIdentity,
                AutoTimestampMode = AutoTimestampMode.Local,
                QuoteIdentifiers = true
            };

        [Fact]
        public void Get_All_Categories_Test()
        {
            using (var repository = new CategoryRepository(Context))
            {
                var categories = repository.GetAll().ToList();
                Assert.True(categories.Count >= 0);
            }
        }

        [Fact]
        public void Get_All_Categories_WithSubcategories_Test()
        {
            using (var repository = new CategoryRepository(Context))
            {
                var entities = repository.UnitOfWork.Categories
                                    .WithAggregate(ExpensesManagerDataModelAggregates.WithSubCategories)
                                    .OrderByDescending(c => c.CategoryName)
                                    .ToList();
                Assert.NotNull(entities);
                Assert.NotNull(entities.First().SubCategories);
                Assert.True(!String.IsNullOrWhiteSpace(entities.First().SubCategories.First().CategoryName));
            }
        }
    }
}

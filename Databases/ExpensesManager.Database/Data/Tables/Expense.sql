﻿CREATE TABLE [Data].[Expense] (
    [Id]            INT            NOT NULL,
    [CategoryId]    INT            NOT NULL,
    [SubCategoryId] INT            NOT NULL,
    [Amount]        SMALLMONEY     CONSTRAINT [DF_Expanse_Amount] DEFAULT ((0)) NOT NULL,
    [Description]   VARCHAR (1024) NULL,
	[Date]     DATETIME  CONSTRAINT [DF_Expanse_Date] DEFAULT (getdate()) NOT NULL,
    [CreatedOn]     DATETIME2 (7)  CONSTRAINT [DF_Expanse_Created] DEFAULT (getdate()) NULL,
    [UpdatedOn]     DATETIME2 (7)  CONSTRAINT [DF_Expanse_Updated] DEFAULT (getdate()) NULL,
    CONSTRAINT [PK_Expense] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_Expense_Category] FOREIGN KEY ([CategoryId]) REFERENCES [Data].[Category] ([Id]),
    CONSTRAINT [FK_Expense_SubCategory] FOREIGN KEY ([SubCategoryId]) REFERENCES [Data].[SubCategory] ([Id])
);


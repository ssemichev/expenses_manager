﻿CREATE TABLE [Data].[Category] (
    [Id]           INT           NOT NULL,
    [CategoryName] VARCHAR (255) NOT NULL,
    [CreatedOn]    DATETIME2 (7) CONSTRAINT [DF_Category_Created] DEFAULT (getdate()) NULL,
    [UpdatedOn]    DATETIME2 (7) CONSTRAINT [DF_Category_Updated] DEFAULT (getdate()) NULL,
    CONSTRAINT [PK_Category] PRIMARY KEY CLUSTERED ([Id] ASC)
);




GO
CREATE UNIQUE NONCLUSTERED INDEX [UI_Category_CategoryName]
    ON [Data].[Category]([CategoryName] ASC);


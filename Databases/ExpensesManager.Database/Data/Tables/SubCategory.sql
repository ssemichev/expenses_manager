﻿CREATE TABLE [Data].[SubCategory] (
    [Id]           INT           NOT NULL,
    [CategoryId]   INT           NOT NULL,
    [CategoryName] VARCHAR (255) NOT NULL,
    [CreatedOn]    DATETIME2 (7) CONSTRAINT [DF_SubCategory_Created] DEFAULT (getdate()) NULL,
    [UpdatedOn]    DATETIME2 (7) CONSTRAINT [DF_SubCategory_Updated] DEFAULT (getdate()) NULL,
    CONSTRAINT [PK_SubCategory] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_Category_SubCategory] FOREIGN KEY ([CategoryId]) REFERENCES [Data].[Category] ([Id])
);




GO



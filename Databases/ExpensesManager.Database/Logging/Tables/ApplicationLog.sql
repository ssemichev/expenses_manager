﻿CREATE TABLE [Logging].[ApplicationLog] (
    [Id]              INT           IDENTITY (1, 1) NOT NULL,
    [LogLevel]        VARCHAR (50)  NOT NULL,
    [Logger]          VARCHAR (500) NOT NULL,
    [Login]           VARCHAR (500) NOT NULL,
    [EntityId]        VARCHAR (100) NOT NULL,
    [SessionId]       VARCHAR (200) NOT NULL,
    [MachineName]     VARCHAR (200) NOT NULL,
    [MachineIp]       VARCHAR (200) NOT NULL,
    [UserIp]          VARCHAR (50)  NOT NULL,
    [ApplicationName] VARCHAR (200) NOT NULL,
    [Url]             VARCHAR (700) NOT NULL,
    [Message]         VARCHAR (MAX) NOT NULL,
    [Details]         VARCHAR (MAX) NOT NULL,
    [Note]            VARCHAR (500) NULL,
    [Created]         DATETIME2 (7) CONSTRAINT [DF_Logger_TaskLog_date] DEFAULT (getdate()) NOT NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC)
);


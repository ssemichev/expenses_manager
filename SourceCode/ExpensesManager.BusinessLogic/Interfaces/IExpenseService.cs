﻿using System;
using System.Collections.Generic;
using ExpensesManager.BusinessLogic.Criteria;
using ExpensesManager.DataAccess.DataModel;

namespace ExpensesManager.BusinessLogic.Interfaces
{
    public interface IExpenseService
    {
        /// <summary>
        /// Does the simple search for Expense by search criteria.
        /// </summary>
        /// <param name="criteria">The search criterion.</param>
        /// <returns>The list of Expenses found.</returns>
        IList<Expense> FindExpenses(ExpenseCriteria criteria);

        Int32 GetTotalExpensesRows(ExpenseCriteria criteria);

        /// <summary>
        /// Create or update the new Expense
        /// </summary>
        /// <param name="expense">The the expense to create or update</param>
        /// <returns>The created or updated expense</returns>
        Expense SaveExpense(Expense expense);
        
        void Delete(Int32 id);
    }
}

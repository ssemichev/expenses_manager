﻿using System;
using System.Collections.Generic;
using ExpensesManager.BusinessLogic.Criteria;
using ExpensesManager.DataAccess.DataModel;

namespace ExpensesManager.BusinessLogic.Interfaces
{
    public interface ICategoryService
    {
        /// <summary>
        /// Does the simple search for Categories by search criteria.
        /// </summary>
        /// <param name="criteria">The search criterion.</param>
        /// <returns>The list of Categories found.</returns>
        IList<Category> FindCategories(CategoryCriteria criteria);

        Int32 GetTotalCategoriesRows(CategoryCriteria criteria);

        /// <summary>
        /// Create a new Category or updates the subCategory if it already exists.
        /// </summary>
        /// <param name="category">The the subCategory to create or update</param>
        /// <returns>The created or updated Category.</returns>
        Category SaveCategory(Category category);

        IList<SubCategory> GetSubcategories(Int32 categoryId);

        /// <summary>
        /// Does the simple search for SubCategories by search criteria.
        /// </summary>
        /// <param name="criteria">The search criterion.</param>
        /// <returns>The list of SubCategories found.</returns>
        IList<SubCategory> FindSubCategories(SubCategoryCriteria criteria);

        Int32 GetTotalSubCategoriesRows(SubCategoryCriteria criterias);

        /// <summary>
        /// Create a new SubCategory or updates the subcategory if it already exists.
        /// </summary>
        /// <param name="subCategory">The the subcategory to create or update</param>
        /// <returns>The created or updated SubCategory.</returns>
        SubCategory SaveSubCategory(SubCategory subCategory);
    }
}

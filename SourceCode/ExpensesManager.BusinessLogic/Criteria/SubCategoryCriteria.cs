﻿using System;

namespace ExpensesManager.BusinessLogic.Criteria
{
    public class SubCategoryCriteria : BaseCriterion
    {
        public Int32 ParentCategoryId { get; set; }
    }
}

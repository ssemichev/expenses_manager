﻿using System;
using ExpensesManager.Common.Interfaces;

namespace ExpensesManager.BusinessLogic.Criteria
{
    /// <summary>
    /// Base class for all search criterias.
    /// </summary>
    public class BaseCriterion : ICriterion
    {
        /// <summary>
        /// Entity Id.
        /// </summary>
        public Int32? EntityId { get; set; }
        /// <summary>
        /// The number of results per page.
        /// </summary>
        public Int32? ResultsPerPage { get; set; }
        /// <summary>
        /// Index of the row, zero-based.
        /// </summary>
        public Int32? RowIndex { get; set; }
        /// <summary>
        /// Sort expression.
        /// </summary>
        public String SortExpression { get; set; }

        #region XmlSerializer tricks

        public bool ShouldSerializeEntityId()
        {
            return EntityId != null;
        }

        public bool ShouldSerializeResultsPerPage()
        {
            return ResultsPerPage != null;
        }

        public bool ShouldSerializeRowIndex()
        {
            return RowIndex != null;
        }

        public bool ShouldSerializeSortExpression()
        {
            return SortExpression != null;
        }

        #endregion

    }
}
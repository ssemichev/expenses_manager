﻿using System;
using System.Collections.Generic;
using System.Linq;
using Castle.Core.Logging;
using ExpensesManager.BusinessLogic.Criteria;
using ExpensesManager.BusinessLogic.Interfaces;
using ExpensesManager.DataAccess.DataModel;
using ExpensesManager.DataAccess.Interfaces;
using Mindscape.LightSpeed;

namespace ExpensesManager.BusinessLogic.Implementation
{
    public class CategoryService : ICategoryService
    {
        private readonly ICategoryRepository _categoryRepository;
        private readonly ISubCategoryRepository _subCategoryRepository;
        private readonly IExtendedLogger _logger = NullLogger.Instance;

        public CategoryService(ICategoryRepository categoryRepository, ISubCategoryRepository subCategoryRepository,
            IExtendedLoggerFactory loggerFactory) 
        {
            _categoryRepository = categoryRepository;
            _subCategoryRepository = subCategoryRepository;
            if (loggerFactory != null) _logger = loggerFactory.Create(GetType());
        }

        #region Implementation of ICategoryService

        /// <summary>
        /// Does the simple search for Categories by search criteria.
        /// </summary>
        /// <param name="criteria">The search criterion.</param>
        /// <returns>The list of Categories found.</returns>
        public IList<Category> FindCategories(CategoryCriteria criteria)
        {
            if (criteria == null || !criteria.EntityId.HasValue)
            {
                return _categoryRepository.GetAll().ToList();
            }

            return _categoryRepository.FindBy(Entity.Attribute("Id") == criteria.EntityId).ToList();
        }

        public Int32 GetTotalCategoriesRows(CategoryCriteria criteria)
        {
            var result = FindCategories(criteria);
            return (result != null) ? result.Count() : 0;
        }

        /// <summary>
        /// Saves the Category.
        /// </summary>
        /// <param name="category">The the subCategory to create or update</param>
        /// <returns>The saved Category.</returns>
        public Category SaveCategory(Category category)
        {
            if (category == null)  { throw new ArgumentNullException("category"); }

            Category originalEntity = null;
            if (category.Id > 0)
            {
                originalEntity = _categoryRepository.GetEntity(category.Id);
            }

            // Create a new subCategory if the subCategory is not exist
            if (originalEntity == null)
            {
                category = _categoryRepository.CreateEntity(category);
                _categoryRepository.Save();
                _subCategoryRepository.CreateEntity(
                    new SubCategory
                        {
                            CategoryId = category.Id, 
                            CategoryName = "Other"
                        });
                _subCategoryRepository.Save();
                return category;
            }

            var updatedCategory = _categoryRepository.UpdateEntity(category);
            _categoryRepository.Save();
            return updatedCategory;
        }

        public IList<SubCategory> GetSubcategories(int categoryId)
        {
            return _categoryRepository.UnitOfWork.SubCategories
                        .Where(sc => sc.CategoryId == categoryId).ToList();
        }

        /// <summary>
        /// Does the simple search for SubCategories by search criteria.
        /// </summary>
        /// <param name="criteria">The search criterion.</param>
        /// <returns>The list of SubCategories found.</returns>
        public IList<SubCategory> FindSubCategories(SubCategoryCriteria criteria)
        {
            return _subCategoryRepository
                .FindBy(Entity.Attribute("Id") == criteria.EntityId 
                        && Entity.Attribute("CategoryId") == criteria.ParentCategoryId).ToList();
        }

        public int GetTotalSubCategoriesRows(SubCategoryCriteria criteria)
        {
            var result = FindSubCategories(criteria);
            return (result != null) ? result.Count() : 0;
        }

        /// <summary>
        /// Create a new SubCategory or updates the subcategory if it already exists.
        /// </summary>
        /// <param name="subCategory">The the subcategory to create or update</param>
        /// <returns>The created or updated SubCategory.</returns>
        public SubCategory SaveSubCategory(SubCategory subCategory)
        {
            if (subCategory == null) { throw new ArgumentNullException("subCategory"); }

            SubCategory originalEntity = null;
            if (subCategory.Id > 0)
            {
                originalEntity = _subCategoryRepository.GetEntity(subCategory.Id);
            }

            // Create a new subcategory if the subCategory is not exist
            if (originalEntity == null)
            {
                subCategory = _subCategoryRepository.CreateEntity(subCategory);
                _subCategoryRepository.Save();
                return subCategory;
            }

            var updatedCategory = _subCategoryRepository.UpdateEntity(subCategory);
            _subCategoryRepository.Save();
            return updatedCategory;
        }

        #endregion
    }
}

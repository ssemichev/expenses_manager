﻿using System;
using System.Collections.Generic;
using System.Linq;
using Castle.Core.Logging;
using ExpensesManager.BusinessLogic.Criteria;
using ExpensesManager.BusinessLogic.Interfaces;
using ExpensesManager.DataAccess.DataModel;
using ExpensesManager.DataAccess.Interfaces;
using Mindscape.LightSpeed;

namespace ExpensesManager.BusinessLogic.Implementation
{
    public class ExpenseService : IExpenseService
    {
        private readonly IExpenseRepository _expenseRepository;
        private readonly IExtendedLogger _logger = NullLogger.Instance;

        public ExpenseService(IExpenseRepository expenseRepository, IExtendedLoggerFactory loggerFactory)
        {
            _expenseRepository = expenseRepository;
            if (loggerFactory != null) _logger = loggerFactory.Create(GetType());
        }

        #region Implementation of IExpenseService

        /// <summary>
        /// Does the simple search for Expense by search criteria.
        /// </summary>
        /// <param name="criteria">The search criterion.</param>
        /// <returns>The list of Expenses found.</returns>
        public IList<Expense> FindExpenses(ExpenseCriteria criteria)
        {
            if (criteria == null || !criteria.EntityId.HasValue)
            {
                return _expenseRepository.GetAllWithDetails();
            }

            return _expenseRepository.FindBy(Entity.Attribute("Id") == criteria.EntityId).ToList();
        }

        public Int32 GetTotalExpensesRows(ExpenseCriteria criteria)
        {
            var result = FindExpenses(criteria);
            return (result != null) ? result.Count() : 0;
        }

        /// <summary>
        /// Create or update the new Expense
        /// </summary>
        /// <param name="expense">The the expense to create or update</param>
        /// <returns>The created or updated expense</returns>
        public Expense SaveExpense(Expense expense)
        {
            if (expense == null) { throw new ArgumentNullException("expense"); }

            Expense originalEntity = null;
            if (expense.Id > 0)
            {
                originalEntity = _expenseRepository.GetEntity(expense.Id);
            }

            // Create a new expense if the expense is not exist
            if (originalEntity == null)
            {
                expense = _expenseRepository.CreateEntity(expense);
                _expenseRepository.Save();
                return expense;
            }

            var updatedExpense = _expenseRepository.UpdateEntity(expense);
            _expenseRepository.Save();
            return updatedExpense;
        }

        public void Delete(int id)
        {
            _expenseRepository.DeleteEntity(id);
        }

        #endregion
    }
}

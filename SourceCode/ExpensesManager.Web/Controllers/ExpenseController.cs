﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Castle.Core.Logging;
using ExpensesManager.BusinessLogic.Criteria;
using ExpensesManager.BusinessLogic.Interfaces;
using ExpensesManager.DataAccess.DataModel;
using ExpensesManager.Web.Models;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;

namespace ExpensesManager.Web.Controllers
{
    [Authorize]
    [HandleError] 
    public class ExpenseController : Controller
    {
        public ILogger Logger { get; set; }
        public IExpenseService ExpenseService { get; set; }

        public ActionResult Index()
        {
            return View("Index", new ExpenseModel());
        }

        /// <summary>
        /// Create the new Expense
        /// </summary>
        /// <param name="expenseModel">Expense to be created</param>
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Create(ExpenseModel expenseModel)
        {
            if (!ModelState.IsValid)
            {
                if (expenseModel.Amount <= 0)
                {
                    return Json(new {success = false, message = "Amount should be greater then zero"});
                }

                if (expenseModel.CategoryId <= 0)
                {
                    return Json(new { success = false, message = "Please select a category" });
                }

                if (expenseModel.SubCategoryId <= 0)
                {
                    return Json(new { success = false, message = "Please select a subcategory" });
                }

                return Json(new { success = false, message = "Validation error" });
            }
            try
            {
                var expense = ExpenseService.SaveExpense(
                    new Expense
                    {
                        CategoryId = expenseModel.CategoryId,
                        SubCategoryId = expenseModel.SubCategoryId,
                        Description = expenseModel.Description.Trim(),
                        Date = expenseModel.Date,
                        Amount = expenseModel.Amount
                    });

                return Json(new { success = true, message = expense.Id });
            }
            catch (Exception ex)
            {
                return Json(new { success = false, message = ex.Message });
            }
        }

        public ActionResult GetExpenses([DataSourceRequest] DataSourceRequest request)
        {
            var expenses = ExpenseService.FindExpenses(new ExpenseCriteria());

            if (expenses != null && expenses.Count > 0)
            {
                return Json(expenses.Select(e =>
                                          new ExpenseModel
                                          {
                                              Id = e.Id,
                                              Amount = e.Amount,
                                              Date = e.Date,
                                              Description = e.Description,
                                              CategoryId = e.CategoryId,
                                              SubCategoryId = e.SubCategoryId,
                                              CategoryName = e.CategoryName,
                                              SubCategoryName = e.SubCategoryName
                                          }).ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
            }
            return Json(new List<ExpenseModel>().ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Delete(ExpenseModel expenseModel)
        {
            try
            {
                ExpenseService.Delete(expenseModel.Id);

                return Json(new { success = true, message = expenseModel.Id });
            }
            catch (Exception ex)
            {
                return Json(new { success = false, message = ex.Message });
            }
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Update(ExpenseModel expenseModel)
        {
            try
            {
                var expense = ExpenseService.SaveExpense(
                    new Expense
                    {
                        Id = expenseModel.Id,
                        CategoryId = expenseModel.CategoryId,
                        SubCategoryId = expenseModel.SubCategoryId,
                        Description = expenseModel.Description.Trim(),
                        Date = expenseModel.Date,
                        Amount = expenseModel.Amount
                    });

                return Json(new { success = true, message = expense.Id });
            }
            catch (Exception ex)
            {
                return Json(new { success = false, message = ex.Message });
            }
        }
    }
}

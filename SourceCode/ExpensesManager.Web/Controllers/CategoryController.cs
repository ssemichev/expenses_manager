﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Castle.Core.Logging;
using ExpensesManager.BusinessLogic.Criteria;
using ExpensesManager.BusinessLogic.Interfaces;
using ExpensesManager.DataAccess.DataModel;
using ExpensesManager.Web.Models;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;

namespace ExpensesManager.Web.Controllers
{
    [Authorize]
    [HandleError] 
    public class CategoryController : Controller
    {
        public ILogger Logger { get; set; }
        public ICategoryService CategoryService { get; set; }

        public ActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// Get the list of categories.
        /// </summary>
        /// <param name="request">DataSourceRequest</param>
        /// <example>// GET: /Category/GetCategories</example>
        public ActionResult GetCategories([DataSourceRequest] DataSourceRequest request)
        {
            var categories = CategoryService.FindCategories(new CategoryCriteria());

            if (categories != null && categories.Count > 0)
            {
                return Json(categories.Select(c =>
                                          new CategoryModel
                                          {
                                              Id = c.Id,
                                              Name = c.CategoryName
                                          }).ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
            }
            return Json(new List<CategoryModel>().ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Adds/updates the new Category
        /// </summary>
        /// <param name="request">DataSourceRequest</param>
        /// <param name="categoryModel">The CategoryModel to add/update</param>
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult SaveCategory([DataSourceRequest] DataSourceRequest request, CategoryModel categoryModel)
        {
            if (categoryModel == null)
            {
                ModelState.AddModelError("Id", "Cannot add/update the category. Try to refresh the browser and add again");
                return Json(ModelState.ToDataSourceResult());
            }

            var isNew = !(categoryModel.Id > 0);

            var category = CategoryService.SaveCategory(
                new Category
                    {
                        Id = categoryModel.Id, 
                        CategoryName = categoryModel.Name
                    });

            categoryModel.Id = category.Id;

            return isNew
                       ? Json(ModelState.ToDataSourceResult())
                       : Json(new List<CategoryModel> {categoryModel}
                                  .ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Get the list of sub categories.
        /// </summary>
        /// <param name="request">DataSourceRequest</param>
        /// <param name="categoryId">Id of parent Category</param>
        /// <example>// GET: /Product/GetSubCategories/categoryId={CategoryId}</example>
        public ActionResult GetSubCategories([DataSourceRequest] DataSourceRequest request, Int32 categoryId)
        {
            var subCategories = CategoryService.GetSubcategories(categoryId);

            if (subCategories != null && subCategories.Count > 0)
            {
                return Json(subCategories.Select(sc =>
                                          new SubCategoryModel()
                                          {
                                              Id = sc.Id,
                                              Name = sc.CategoryName
                                          }).ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
            }
            return Json(new List<SubCategoryModel>().ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Get the list of sub categories.
        /// </summary>
        /// <param name="request">DataSourceRequest</param>
        /// <example>// GET: /Product/GetFilteredSubCategories/</example>
        public ActionResult GetFilteredSubCategories([DataSourceRequest] DataSourceRequest request)
        {
            var categoryId = Int32.Parse(System.Web.HttpContext.Current.Request.Params["filter[filters][0][value]"]);
            var subCategories = CategoryService.GetSubcategories(categoryId);

            if (subCategories != null && subCategories.Count > 0)
            {
                return Json(subCategories.Select(sc =>
                                          new SubCategoryModel()
                                          {
                                              Id = sc.Id,
                                              Name = sc.CategoryName
                                          }).ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
            }
            return Json(new List<SubCategoryModel>().ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Adds/updates the new SubCategory
        /// </summary>
        /// <param name="request">DataSourceRequest</param>
        /// <param name="categoryId">Id of parent Category</param>
        /// <param name="subCategoryModel">The SubCategoryModel to add/update</param>
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult SaveSubCategory([DataSourceRequest] DataSourceRequest request, Int32 categoryId, SubCategoryModel subCategoryModel)
        {
            if (subCategoryModel == null)
            {
                ModelState.AddModelError("Id", "Cannot add/update the option. Try to refresh the browser and add again");
                return Json(ModelState.ToDataSourceResult());
            }

            var isNew = !(subCategoryModel.Id > 0);

            var subCategory = CategoryService.SaveSubCategory(new SubCategory
            {
                Id = subCategoryModel.Id,
                CategoryId = categoryId,
                CategoryName = subCategoryModel.Name
            });

            subCategoryModel.Id = subCategory.Id;

            return isNew 
                ? Json(ModelState.ToDataSourceResult()) 
                : Json(new List<SubCategoryModel> { subCategoryModel }
                    .ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }
    }
}

﻿var Layout = {};

Layout.HandleKendoGridSaveChanges = function (e, grid) {
    var valid = true;
    // See if there are any insert rows
    var rows = grid.tbody.find("tr");
    for (var i = 0; i < rows.length; i++) {

        // Get the model
        var model = grid.dataItem(rows[i]);
        // Check the id property - this will indicate an insert row
        if (model && model.Id <= 0 && valid) {

            // Loop through the columns and validate them
            var cols = $(rows[i]).find("td");
            for (var j = 0; j < cols.length; j++) {
                // Put cell into edit mode
                grid.editCell($(cols[j]));

                if (grid != null && grid.editable != null) {
                    // By calling editable end we will make validation fire
                    if (!grid.editable.end()) {
                        valid = false;
                        break;
                    } else {
                        // Take cell out of edit mode
                        grid.closeCell();
                    }
                }
            }
        }
        else {
            // We're now to existing rows or have a validation error so we can stop
            break;
        }
    }

    if (!valid) {
        // Abort the save operation
        e.preventDefault(true);
    }
};

Layout.ShowResultsWindow = function (title, confirmationText) {
    $('#genericConfirmationWindow').dialog('option', 'buttons', {
        "Ok": function () { $(this).dialog("close"); }
    });
    $('#genericConfirmationWindow').dialog('option', 'title', 'title');
    $('#genericConfirmationWindowText').html(confirmationText);
    $('#genericConfirmationWindow').dialog("open");
};

function DisableEntityConfirmationInit() {
    $('#disableEntityCofirmation').dialog({
        open: function () {
            $('.ui-dialog-buttonpane button:eq(1)').focus();
        },
        autoOpen: false,
        width: 600,
        modal: true,
        resizable: false
    });
};

Layout.ConfirmationWindowsInit = function () {
    $('#genericConfirmationWindow').dialog({
        open: function () {
            $('.ui-dialog-buttonpane button:eq(1)').focus();
        },
        autoOpen: false,
        width: 600,
        modal: true,
        resizable: true
    });
};


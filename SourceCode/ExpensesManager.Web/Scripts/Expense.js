﻿var Expense = {};
Expense._subCategoryDataSource = null;
Expense._expensesDataSource = null;

Expense.init = function () {

    $('#submitButton').click(function () {
        $('form#expenseForm').submit();
    });
    
    $("#Amount").kendoNumericTextBox({ format: "c", decimals: 3, min: 0, max: 100000, step: 1 });
    $("#Date").kendoDatePicker();

    $("#CategoryId").kendoComboBox({
        autoBind: false,
        filter: "contains",
        placeholder: "Select category...",
        dataTextField: "Name",
        dataValueField: "Id",
        dataSource: {
            type: "json",
            serverFiltering: true,
            transport: {
                read: window._rootUrl + "Category/GetCategories",
            },
            batch: false,
            sort: { field: "Name", dir: "asc" },
            schema: {
                data: "Data",
                total: "Total",
                errors: "Errors",
                model: {
                    id: "Id",
                    fields: {
                        Id: { type: "number" },
                        Name: { type: "string" }
                    }
                }
            }
        }
    }).data("kendoComboBox");
    
    $("#SubCategoryId").kendoComboBox({
        autoBind: false,
        cascadeFrom: "CategoryId",
        filter: "contains",
        placeholder: "Select subcategory...",
        dataTextField: "Name",
        dataValueField: "Id",
        dataSource: {
            type: "json",
            serverFiltering: true,
            transport: {
                read: window._rootUrl + "Category/GetFilteredSubCategories"
            },
            batch: false,
            sort: { field: "Name", dir: "asc" },
            schema: {
                data: "Data",
                total: "Total",
                errors: "Errors",
                model: {
                    id: "Id",
                    fields: {
                        Id: { type: "number" },
                        Name: { type: "string" }
                    }
                }
            }
        }
    }).data("kendoComboBox");
    
    Expense.ConfirmationWindowsInit();
    Expense.ExpenseGridInit();
};

Expense.OnSuccess = function(response) {
    if (response.success) {
        var str = "Your record has been added. Date: " + $("#Date").val() + " Amount: $" + $("#Amount").val() + " Description: " + $("#Description").val();
        $("#errorText").text(str);
        $("#Description").val("");
        $("#Amount").val("0.00");
        $("#Date").val($.datepicker.formatDate('mm/dd/yyyy', new Date()));
        $('#CategoryId').empty();
        $('#SubCategoryId').empty();
        Expense.init();
        
    } else {
        $("#errorText").text(response.message);
    }
};

Expense.ExpenseGridInit = function () {
    Expense._expensesDataSource = new kendo.data.DataSource({
        transport: {
            read: {
                url: window._rootUrl + "Expense/GetExpenses",
                dataType: "json"
            },
            update: {
                type: "POST",
                url: window._rootUrl + "Expense/Update",
                dataType: "json"
            },
            destroy: {
                type: "POST",
                url: window._rootUrl + "Expense/Delete",
                dataType: "json"
            },
            parameterMap: function (options, operation) {
                if (operation !== "read" && options.models) {
                    return { models: kendo.stringify(options.models) };
                } else {
                    return options;
                }
            }
        },
        error: function (args) {
            if (args.errors) {
                var grid = $("#ExpensesGrid").data("kendoGrid");
                grid.one("dataBinding", function (e) {
                    e.preventDefault();   // cancel grid rebind if error occurs                             

                    var errorMessage = "";
                    for (var error in args.errors) {
                        errorMessage += " " + args.errors[error].errors;
                        Expense.ShowErrorWindow(errorMessage);
                    }
                });
            } else {
                Expense.ShowErrorWindow("Cannot synchronize Expenses grid.");
            }
            this.cancelChanges();
        },
        batch: false,
        pageSize: 15,
        sort: { field: "Date", dir: "desc" },
        schema: {
            data: "Data",
            total: "Total",
            errors: "Errors",
            model: {
                id: "Id",
                fields: {
                    Id: { editable: false, type: "number" },
                    Date: { validation: { required: true }, type: "date" },
                    Description: { validation: { required: true } },
                    CategoryName: { editable: false },
                    CategoryId: { type: "number" },
                    SubCategoryId: { type: "number" },
                    SubCategoryName: { editable: false },
                    Amount: { type: "number", validation: { min: 0.01, required: true } }
                }
            }
        }
    });
    
    $("#ExpensesGrid").kendoGrid({
        dataSource: Expense._expensesDataSource,
        editable: "inline",
        pageable: true,
        navigatable: true,
        scrollable: false,
        sortable: true,
        columns: [
            { field: "Date", title: "Date", width: 120, format: "{0:MM/dd/yyyy}" },
            { field: "CategoryName", title: "CategoryName" },
            { field: "SubCategoryName", title: "SubCategoryName" },
            { field: "Amount", title: "Amount", width: 150, format: "{0:c}" },
            { field: "Description", title: "Description" },
            { command: ["edit", "destroy"], title: "&nbsp;", width: "210px" }],
        saveChanges: function (e) {
            Layout.HandleKendoGridSaveChanges(e, this);
        }
    });

    $("#CategoriesGrid").kendoGrid({
        dataSource: Category._categoryDataSource,
        editable: "inline",
        pageable: true,
        navigatable: true,
        scrollable: false,
        sortable: true,
        toolbar: ["create"],
        detailInit: Category.SubCategoriesInit,
        columns: [
            { field: "Name", title: "Name" },
            { command: ["edit"], title: "&nbsp;", width: "210px" }],
        saveChanges: function (e) {
            Layout.HandleKendoGridSaveChanges(e, this);
        }
    });
};

Expense.ShowErrorWindow = function (error) {
    $('#confirmationWindow').dialog('option', 'buttons', {
        "Ok": function () { $(this).dialog("close"); }
    });
    $('#confirmationWindow').dialog('option', 'title', 'Error');

    $('#confirmationWindowText').html("<strong style='color:red'>Error! " + error + "</strong><br /><br />Please try again later. See logs for details.");

    $('#confirmationWindow').dialog("open");
};

Expense.ConfirmationWindowsInit = function () {
    $('#confirmationWindow').dialog({
        open: function () {
            $('.ui-dialog-buttonpane button:eq(1)').focus();
        },
        autoOpen: false,
        width: 600,
        modal: true,
        resizable: true
    });
};

Expense.OnFaliure = function() {
    $("#errorText").text("Error. Please try later.");
};

function ajaxValidate() {
    return $('expenseForm').validate();
}


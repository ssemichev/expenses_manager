﻿var Category = {};

Category._categoryDataSource = null;
Category._subCategoryDataSource = null;

Category.init = function () {
    Category.ConfirmationWindowsInit();
    Category.CategoryGridInit();
};

Category.ShowErrorWindow = function (error) {
    $('#confirmationWindow').dialog('option', 'buttons', {
        "Ok": function () { $(this).dialog("close"); }
    });
    $('#confirmationWindow').dialog('option', 'title', 'Error');

    $('#confirmationWindowText').html("<strong style='color:red'>Error! " + error + "</strong><br /><br />Please try again later. See logs for details.");
    
    $('#confirmationWindow').dialog("open");
};

Category.ConfirmationWindowsInit = function () {
    $('#confirmationWindow').dialog({
        open: function () {
            $('.ui-dialog-buttonpane button:eq(1)').focus();
        },
        autoOpen: false,
        width: 600,
        modal: true,
        resizable: true
    });
};

Category.CategoryGridInit = function() {
    
        Category._categoryDataSource = new kendo.data.DataSource({
            transport: {
                read: {
                    url: window._rootUrl + "Category/GetCategories",
                    dataType: "json"
                },
                update: {
                    type: "POST",
                    url: window._rootUrl + "Category/SaveCategory",
                    dataType: "json"
                },
                create: {
                    type: "POST",
                    url: window._rootUrl + "Category/SaveCategory",
                    dataType: "json",
                    complete: function () { Category._categoryDataSource.read(); }
                },
                parameterMap: function (options, operation) {
                    if (operation !== "read" && options.models) {
                        return { models: kendo.stringify(options.models) };
                    } else {
                        return options;
                    }
                }
            },
            error: function (args) {
                if (args.errors) {
                    var grid = $("#CategoriesGrid").data("kendoGrid");
                    grid.one("dataBinding", function (e) {
                        e.preventDefault();   // cancel grid rebind if error occurs                             

                        var errorMessage = "";
                        for (var error in args.errors) {
                            errorMessage += " " + args.errors[error].errors;
                            Category.ShowErrorWindow(errorMessage);
                        }
                    });
                } else {
                    Category.ShowErrorWindow("Cannot synchronize Categories grid.");
                }
                this.cancelChanges();
            },
            batch: false,
            pageSize: 15,
            sort: { field: "Name", dir: "asc" },
            schema: {
                data: "Data",
                total: "Total",
                errors: "Errors",
                model: {
                    id: "Id",
                    fields: {
                        Id: { editable: false, type: "number" },
                       Name: { validation: { required: true } }
                    }
                }
            }
        });

        $("#CategoriesGrid").kendoGrid({
            dataSource: Category._categoryDataSource,
            editable: "inline",
            pageable: true,
            navigatable: true,
            scrollable: false,
            sortable: true,
            toolbar: ["create"],
            detailInit: Category.SubCategoriesInit,
            columns: [
                { field: "Name", title: "Name" },
                { command: ["edit"], title: "&nbsp;", width: "210px" }],
            saveChanges: function (e) {
                Layout.HandleKendoGridSaveChanges(e, this);
            }
        });
};


Category.SubCategoriesInit = function (e) {
    var optionsDataSource = new kendo.data.DataSource(
        {
            transport: {
                read: {
                    url: window._rootUrl + "Category/GetSubCategories?categoryId=" + e.data.Id,
                    dataType: "json"
                },
                update: {
                    type: "POST",
                    url: window._rootUrl + "Category/SaveSubCategory?categoryId=" + e.data.Id,
                    dataType: "json"
                },
                create: {
                    type: "POST",
                    url: window._rootUrl + "Category/SaveSubCategory?categoryId=" + e.data.Id,
                    dataType: "json"
                },
                parameterMap: function (options, operation) {
                    if (operation !== "read" && options.models) {
                        return { models: kendo.stringify(options.models) };
                    } else {
                        return options;
                    }
                }
            },
            error: function (args) {
                if (args.errors) {
                    var errorMessage = "";
                    for (var error in args.errors) {
                        errorMessage += " " + args.errors[error].errors;
                        Category.ShowErrorWindow(errorMessage);
                    }
                } else {
                    Category.ShowErrorWindow("Cannot synchronize SubCategory grid.");
                }
                this.cancelChanges();
                optionsDataSource.read();
                Category._subCategoryDataSource.read();
            },
            batch: false,
            pageSize: 15,
            sort: { field: "Value", dir: "desc" },
            schema: {
                data: "Data",
                total: "Total",
                errors: "Errors",
                model: {
                    id: "Id",
                    fields: {
                        Id: { editable: false, type: "number" },
                        Name: { validation: { required: true } }
                    }
                }
            }
        }
    );

    $("<div/>").appendTo(e.detailCell).kendoGrid({
        dataSource: optionsDataSource,
        editable: "inline",
        pageable: true,
        navigatable: true,
        scrollable: false,
        sortable: true,
        confirmation: false,
        toolbar: ["create"],
        columns: [
            { field: "Name", title: "Name" },
            { command: ["edit"], title: "&nbsp;", width: "210px" }],
        saveChanges: function (arg) {
            Layout.HandleKendoGridSaveChanges(arg, this);
        }
    });
};

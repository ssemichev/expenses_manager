﻿using System;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using ExpensesManager.Common.IoC;
using ExpensesManager.Web.IoC.Installers;

namespace ExpensesManager.Web
{
    public class MvcApplication : HttpApplication
    {
        public static IServiceLocator Container { get; set; }

        protected void Application_Start()
        {
            //Initialize IoC
            Container = Bootstrapper.ConfigureContainer();

            AreaRegistration.RegisterAllAreas();

            WebApiConfig.Register(GlobalConfiguration.Configuration);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            AuthConfig.RegisterAuth();
        }

        protected void Application_End()
        {
            if (Container == null) return;
            var disposable = Container as IDisposable;
            if (disposable != null) disposable.Dispose();
        }
    }
}
﻿using System;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace ExpensesManager.Web.Extensions
{
    public class JsonParameterAttribute : ActionFilterAttribute
    {
        public string Input { get; set; }
        public string Output { get; set; }
        public Type DataType { get; set; }

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            var json = filterContext.HttpContext.Request.Params[Input];
            filterContext.ActionParameters[Output] = new JavaScriptSerializer().Deserialize(json, DataType);
        }
    }
}
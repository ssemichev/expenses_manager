﻿using System.Security.Principal;
using System.Web;
using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;

namespace ExpensesManager.Web.IoC.Installers
{
    /// <summary>
    /// Instructs <c>Castle Windsor</c> how to create security conserns
    /// </summary>
    public class SecurityConsernsInstaller : IWindsorInstaller
    {
        /// <summary>
        /// Performs the installation in the <see cref="T:Castle.Windsor.IWindsorContainer"/>.
        /// </summary>
        /// <param name="container">The container.</param><param name="store">The configuration store.</param>
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container.Register(Component.For<IIdentity>().LifeStyle.PerWebRequest
                .UsingFactoryMethod(() => HttpContext.Current.User.Identity));

            container.Register(Component.For<IPrincipal>().LifeStyle.PerWebRequest
                .UsingFactoryMethod(() => HttpContext.Current.User));

            container.Register(Component.For<HttpContextBase>().LifeStyle.PerWebRequest
                .UsingFactoryMethod(() => new HttpContextWrapper(HttpContext.Current)));
        }
    }
}
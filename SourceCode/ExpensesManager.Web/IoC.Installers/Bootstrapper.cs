﻿using System.Web.Mvc;
using Castle.Windsor;
using Castle.Windsor.Installer;
using ExpensesManager.Common.IoC;
using ExpensesManager.Web.Infrastructure;

namespace ExpensesManager.Web.IoC.Installers
{
    public static class Bootstrapper
    {
        /// <summary>
        /// Configures IoC container with BL support classes.
        /// </summary>
        public static IServiceLocator ConfigureContainer()
        {
            var serviceLocator = new WindsorServiceLocator();
            var container = new WindsorContainer().Install(FromAssembly.This());

            var controllerFactory = new WindsorControllerFactory(container.Kernel);
            ControllerBuilder.Current.SetControllerFactory(controllerFactory);

            serviceLocator.Initialize(container);
            return serviceLocator;
        }
    }
}
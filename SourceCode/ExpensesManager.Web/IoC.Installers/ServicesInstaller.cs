﻿using Castle.Core;
using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;

namespace ExpensesManager.Web.IoC.Installers
{
    /// <summary>
    /// Instructs <c>Castle Windsor</c> how to create services
    /// </summary>
    public class ServicesInstaller : IWindsorInstaller
    {
        /// <summary>
        /// Performs the installation in the <see cref="T:Castle.Windsor.IWindsorContainer"/>.
        /// </summary>
        /// <param name="container">The container.</param><param name="store">The configuration store.</param>
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container.Register(Classes.FromAssemblyNamed("ExpensesManager.BusinessLogic")
                .InNamespace("ExpensesManager.BusinessLogic.Implementation").WithService.DefaultInterfaces()
                .LifestylePerWebRequest()
                .Configure(c => c.Interceptors(InterceptorReference.ForKey("LoggingDebugAspect"), 
                    InterceptorReference.ForKey("ExceptionHandlingAspect")).Anywhere.LifestyleSingleton()));
        }
    }
}
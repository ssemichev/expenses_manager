﻿using Castle.Core.Logging;
using Castle.DynamicProxy;
using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using ExpensesManager.Common.Aspects;

namespace ExpensesManager.Web.IoC.Installers
{
    /// <summary>
    /// Instructs <c>Castle Windsor</c> how to create aspects
    /// </summary>
    public class AspectsInstaller : IWindsorInstaller
    {
        /// <summary>
        /// Performs the installation in the <see cref="T:Castle.Windsor.IWindsorContainer"/>.
        /// </summary>
        /// <param name="container">The container.</param><param name="store">The configuration store.</param>
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container.Register(Component.For<IInterceptor>().ImplementedBy<LoggingAspect>().Named("LoggingDebugAspect"));

            container.Register(Component.For<IInterceptor>().ImplementedBy<LoggingAspect>()
                .UsingFactoryMethod(() => new LoggingAspect(container.Resolve<IExtendedLoggerFactory>()) { LoggerLevel = LoggerLevel.Info })
                .Named("LoggingInfoAspect"));

            container.Register(Component.For<IInterceptor>().ImplementedBy<ExceptionHandlingAspect>().Named("ExceptionHandlingAspect"));
            container.Register(Component.For<IInterceptor>().ImplementedBy<SuppressExceptionHandlingAspect>().Named("ExceptionHandlingWithSuppressionAspect"));
        }
    }
}
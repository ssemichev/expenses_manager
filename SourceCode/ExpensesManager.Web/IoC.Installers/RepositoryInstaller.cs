﻿using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using ExpensesManager.DataAccess.DataModel;
using Mindscape.LightSpeed;
using Mindscape.LightSpeed.Audit;

namespace ExpensesManager.Web.IoC.Installers
{
    /// <summary>
    /// Instructs <c>Castle Windsor</c> how to create repositories
    /// </summary>
    public class RepositoryInstaller : IWindsorInstaller
    {
        /// <summary>
        /// Performs the installation in the <see cref="T:Castle.Windsor.IWindsorContainer"/>.
        /// </summary>
        /// <param name="container">The container.</param><param name="store">The configuration store.</param>
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container.Register(Component.For<LightSpeedContext<ExpensesManagerDataModelUnitOfWork>>().LifeStyle.Singleton
                .UsingFactoryMethod(() => new LightSpeedContext<ExpensesManagerDataModelUnitOfWork>("default")
                {
                    AuditInfoMode = AuditInfoMode.HttpContext,
                    AutoTimestampMode = AutoTimestampMode.Local,
                    QuoteIdentifiers = true
                }));

            container.Register(Classes.FromAssemblyNamed("ExpensesManager.DataAccess")
                .InNamespace("ExpensesManager.DataAccess.Implementation")
                .WithService.DefaultInterfaces()
                .LifestylePerWebRequest());
        }
    }
}
﻿using System;

namespace ExpensesManager.Web.Models
{
    public class CategoryModel
    {
        public Int32 Id { get; set; }
        public String Name { get; set; }
    }
}
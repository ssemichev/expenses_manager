﻿using System;

namespace ExpensesManager.Web.Models
{
    public class SubCategoryModel
    {
        public Int32 Id { get; set; }
        public String Name { get; set; }
    }
}
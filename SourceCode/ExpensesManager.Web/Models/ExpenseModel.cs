﻿using System;
using System.ComponentModel.DataAnnotations;

namespace ExpensesManager.Web.Models
{
    public class ExpenseModel
    {
        public ExpenseModel()
        {
            Date = DateTime.Now;
            Amount = 0;
        }

        public Int32 Id { get; set; }
        [Required]
        public DateTime Date { get; set; }
        [Required]
        [StringLength(1024)]
        public String Description { get; set; }
        public String CategoryName { get; set; }
        public String SubCategoryName { get; set; }
        [Required]
        [Range(0.01, Double.MaxValue, ErrorMessage = "Amount should be greater then zero")]
        public Decimal Amount { get; set; }
        [Required(ErrorMessage = "Please select a category")]
        public Int32 CategoryId { get; set; }
        [Required(ErrorMessage = "Please select a subcategory")]
        public Int32 SubCategoryId { get; set; }
    }
}
using System;

using Mindscape.LightSpeed;
using Mindscape.LightSpeed.Validation;
using Mindscape.LightSpeed.Linq;

namespace ExpensesManager.DataAccess.DataModel
{
  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  [Table(Schema="Data")]
  public partial class Category : Entity<int>
  {
    #region Fields
  
    [ValidatePresence]
    [ValidateLength(0, 255)]
    private string _categoryName;

    #pragma warning disable 649  // "Field is never assigned to" - LightSpeed assigns these fields internally
    private readonly System.DateTime _createdOn;
    private readonly System.DateTime _updatedOn;
    #pragma warning restore 649    

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the CategoryName entity attribute.</summary>
    public const string CategoryNameField = "CategoryName";
    /// <summary>Identifies the CreatedOn entity attribute.</summary>
    public const string CreatedOnField = "CreatedOn";
    /// <summary>Identifies the UpdatedOn entity attribute.</summary>
    public const string UpdatedOnField = "UpdatedOn";


    #endregion
    
    #region Relationships

    [EagerLoad(AggregateName="WithSubCategories")]
    [ReverseAssociation("Category")]
    private readonly EntityCollection<SubCategory> _subCategories = new EntityCollection<SubCategory>();
    [ReverseAssociation("Category")]
    private readonly EntityCollection<Expense> _expensesByCategory = new EntityCollection<Expense>();


    #endregion
    
    #region Properties

    [System.Diagnostics.DebuggerNonUserCode]
    public EntityCollection<SubCategory> SubCategories
    {
      get { return Get(_subCategories); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public EntityCollection<Expense> ExpensesByCategory
    {
      get { return Get(_expensesByCategory); }
    }


    [System.Diagnostics.DebuggerNonUserCode]
    public string CategoryName
    {
      get { return Get(ref _categoryName, "CategoryName"); }
      set { Set(ref _categoryName, value, "CategoryName"); }
    }
    /// <summary>Gets the time the entity was created</summary>
    [System.Diagnostics.DebuggerNonUserCode]
    public System.DateTime CreatedOn
    {
      get { return _createdOn; }   
    }

    /// <summary>Gets the time the entity was last updated</summary>
    [System.Diagnostics.DebuggerNonUserCode]
    public System.DateTime UpdatedOn
    {
      get { return _updatedOn; }   
    }

    #endregion
  }


  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  [Table(Schema="Data")]
  public partial class SubCategory : Entity<int>
  {
    #region Fields
  
    [ValidatePresence]
    [ValidateLength(0, 255)]
    private string _categoryName;
    private int _categoryId;

    #pragma warning disable 649  // "Field is never assigned to" - LightSpeed assigns these fields internally
    private readonly System.DateTime _createdOn;
    private readonly System.DateTime _updatedOn;
    #pragma warning restore 649    

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the CategoryName entity attribute.</summary>
    public const string CategoryNameField = "CategoryName";
    /// <summary>Identifies the CategoryId entity attribute.</summary>
    public const string CategoryIdField = "CategoryId";
    /// <summary>Identifies the CreatedOn entity attribute.</summary>
    public const string CreatedOnField = "CreatedOn";
    /// <summary>Identifies the UpdatedOn entity attribute.</summary>
    public const string UpdatedOnField = "UpdatedOn";


    #endregion
    
    #region Relationships

    [ReverseAssociation("SubCategory")]
    private readonly EntityCollection<Expense> _expensesBySubCategory = new EntityCollection<Expense>();
    [ReverseAssociation("SubCategories")]
    private readonly EntityHolder<Category> _category = new EntityHolder<Category>();


    #endregion
    
    #region Properties

    [System.Diagnostics.DebuggerNonUserCode]
    public EntityCollection<Expense> ExpensesBySubCategory
    {
      get { return Get(_expensesBySubCategory); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public Category Category
    {
      get { return Get(_category); }
      set { Set(_category, value); }
    }


    [System.Diagnostics.DebuggerNonUserCode]
    public string CategoryName
    {
      get { return Get(ref _categoryName, "CategoryName"); }
      set { Set(ref _categoryName, value, "CategoryName"); }
    }

    /// <summary>Gets or sets the ID for the <see cref="Category" /> property.</summary>
    [System.Diagnostics.DebuggerNonUserCode]
    public int CategoryId
    {
      get { return Get(ref _categoryId, "CategoryId"); }
      set { Set(ref _categoryId, value, "CategoryId"); }
    }
    /// <summary>Gets the time the entity was created</summary>
    [System.Diagnostics.DebuggerNonUserCode]
    public System.DateTime CreatedOn
    {
      get { return _createdOn; }   
    }

    /// <summary>Gets the time the entity was last updated</summary>
    [System.Diagnostics.DebuggerNonUserCode]
    public System.DateTime UpdatedOn
    {
      get { return _updatedOn; }   
    }

    #endregion
  }


  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  [Table(Schema="Data")]
  public partial class Expense : Entity<int>
  {
    #region Fields
  
    private decimal _amount;
    [ValidateLength(0, 1024)]
    private string _description;
    private System.DateTime _date;
    private int _categoryId;
    private int _subCategoryId;

    #pragma warning disable 649  // "Field is never assigned to" - LightSpeed assigns these fields internally
    private readonly System.DateTime _createdOn;
    private readonly System.DateTime _updatedOn;
    #pragma warning restore 649    

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the Amount entity attribute.</summary>
    public const string AmountField = "Amount";
    /// <summary>Identifies the Description entity attribute.</summary>
    public const string DescriptionField = "Description";
    /// <summary>Identifies the Date entity attribute.</summary>
    public const string DateField = "Date";
    /// <summary>Identifies the CategoryId entity attribute.</summary>
    public const string CategoryIdField = "CategoryId";
    /// <summary>Identifies the SubCategoryId entity attribute.</summary>
    public const string SubCategoryIdField = "SubCategoryId";
    /// <summary>Identifies the CreatedOn entity attribute.</summary>
    public const string CreatedOnField = "CreatedOn";
    /// <summary>Identifies the UpdatedOn entity attribute.</summary>
    public const string UpdatedOnField = "UpdatedOn";


    #endregion
    
    #region Relationships

    [EagerLoad(AggregateName="WithExpenseDetails")]
    [ReverseAssociation("ExpensesByCategory")]
    private readonly EntityHolder<Category> _category = new EntityHolder<Category>();
    [EagerLoad(AggregateName="WithExpenseDetails")]
    [ReverseAssociation("ExpensesBySubCategory")]
    private readonly EntityHolder<SubCategory> _subCategory = new EntityHolder<SubCategory>();


    #endregion
    
    #region Properties

    [System.Diagnostics.DebuggerNonUserCode]
    public Category Category
    {
      get { return Get(_category); }
      set { Set(_category, value); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public SubCategory SubCategory
    {
      get { return Get(_subCategory); }
      set { Set(_subCategory, value); }
    }


    [System.Diagnostics.DebuggerNonUserCode]
    public decimal Amount
    {
      get { return Get(ref _amount, "Amount"); }
      set { Set(ref _amount, value, "Amount"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string Description
    {
      get { return Get(ref _description, "Description"); }
      set { Set(ref _description, value, "Description"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.DateTime Date
    {
      get { return Get(ref _date, "Date"); }
      set { Set(ref _date, value, "Date"); }
    }

    /// <summary>Gets or sets the ID for the <see cref="Category" /> property.</summary>
    [System.Diagnostics.DebuggerNonUserCode]
    public int CategoryId
    {
      get { return Get(ref _categoryId, "CategoryId"); }
      set { Set(ref _categoryId, value, "CategoryId"); }
    }

    /// <summary>Gets or sets the ID for the <see cref="SubCategory" /> property.</summary>
    [System.Diagnostics.DebuggerNonUserCode]
    public int SubCategoryId
    {
      get { return Get(ref _subCategoryId, "SubCategoryId"); }
      set { Set(ref _subCategoryId, value, "SubCategoryId"); }
    }
    /// <summary>Gets the time the entity was created</summary>
    [System.Diagnostics.DebuggerNonUserCode]
    public System.DateTime CreatedOn
    {
      get { return _createdOn; }   
    }

    /// <summary>Gets the time the entity was last updated</summary>
    [System.Diagnostics.DebuggerNonUserCode]
    public System.DateTime UpdatedOn
    {
      get { return _updatedOn; }   
    }

    #endregion
  }



  public static partial class ExpensesManagerDataModelAggregates
  {
    public const string WithSubCategories = @"WithSubCategories";
    public const string WithExpenseDetails = @"WithExpenseDetails";
  }

  /// <summary>
  /// Provides a strong-typed unit of work for working with the ExpensesManagerDataModel model.
  /// </summary>
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  public partial class ExpensesManagerDataModelUnitOfWork : Mindscape.LightSpeed.UnitOfWork
  {

    public System.Linq.IQueryable<Category> Categories
    {
      get { return this.Query<Category>(); }
    }
    
    public System.Linq.IQueryable<SubCategory> SubCategories
    {
      get { return this.Query<SubCategory>(); }
    }
    
    public System.Linq.IQueryable<Expense> Expenses
    {
      get { return this.Query<Expense>(); }
    }
    
  }

}

﻿using System;

namespace ExpensesManager.DataAccess.DataModel
{
    partial class Expense
    {
        public String CategoryName
        {
            get
            {
                return Category == null ? String.Empty : Category.CategoryName;
            }
        }

        public String SubCategoryName
        {
            get
            {
                return SubCategory == null ? String.Empty : SubCategory.CategoryName;
            }
        }
    }
}

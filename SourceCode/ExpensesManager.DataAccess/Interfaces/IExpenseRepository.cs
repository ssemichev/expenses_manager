﻿using System;
using System.Collections.Generic;
using ExpensesManager.Common.Interfaces;
using ExpensesManager.DataAccess.DataModel;
using Mindscape.LightSpeed;

namespace ExpensesManager.DataAccess.Interfaces
{
    public interface IExpenseRepository : IEntity<Expense, Int32>, 
        IRepository<ExpensesManagerDataModelUnitOfWork, LightSpeedContext<ExpensesManagerDataModelUnitOfWork>>
    {
        IList<Expense> GetAllWithDetails();
    }
}

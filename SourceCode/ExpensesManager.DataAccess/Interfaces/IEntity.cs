﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using Mindscape.LightSpeed.Querying;

namespace ExpensesManager.DataAccess.Interfaces
{
    /// <summary>
    /// Implement CRUD for business entities
    /// </summary>
    /// <typeparam name="T">The type of the business entity</typeparam>
    /// <typeparam name="TU">The type of the ID</typeparam>
    public interface IEntity<T, in TU> : IDisposable
    {
        /// <summary>
        /// Retrieves business entity
        /// </summary>       
        /// <param name="entityId">The entity id</param>
        /// <returns>The business entity</returns>
        T GetEntity(TU entityId);
        /// <summary>
        /// Returns all existing entities
        /// </summary>
        IEnumerable<T> GetAll();
        /// <summary>
        /// Searches for enities that match the conditions defined by the specified predicate
        /// </summary>
        IEnumerable<T> FindBy(Expression<Func<T, Boolean>> predicate);
        /// <summary>
        /// Searches for enities that match the conditions defined by the specified predicate
        /// </summary>
        IEnumerable<T> FindBy(QueryExpression predicate);
        /// <summary>
        /// Insert a new entity
        /// </summary>
        /// <param name="entity">The business entity</param>
        /// <returns>The business entity</returns>
        T CreateEntity(T entity);
        /// <summary>
        /// Update an existing entity
        /// </summary>
        /// <param name="entity">The business entity</param>
        /// <returns>The business entity</returns>
        T UpdateEntity(T entity);
        /// <summary>
        /// Delete an existing entity
        /// </summary>
        /// <param name="entityId">The entity id</param>
        void DeleteEntity(TU entityId);
    }
}

﻿using System;
using ExpensesManager.Common.Interfaces;
using ExpensesManager.DataAccess.DataModel;
using Mindscape.LightSpeed;

namespace ExpensesManager.DataAccess.Interfaces
{
    public interface ICategoryRepository : IEntity<Category, Int32>, 
        IRepository<ExpensesManagerDataModelUnitOfWork, LightSpeedContext<ExpensesManagerDataModelUnitOfWork>>
    { }
}

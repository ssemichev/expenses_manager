﻿using System;
using System.Collections.Generic;
using System.Linq;
using ExpensesManager.DataAccess.DataModel;
using ExpensesManager.DataAccess.Interfaces;
using Mindscape.LightSpeed;
using Mindscape.LightSpeed.Linq;

namespace ExpensesManager.DataAccess.Implementation
{
    public class ExpenseRepository : WebApplicationRepository<Expense, Int32>, IExpenseRepository
    {
        public ExpenseRepository(LightSpeedContext<ExpensesManagerDataModelUnitOfWork> context)
            : base(context)
        {
        }

        #region Implementation of IExpenseRepository

        public IList<Expense> GetAllWithDetails()
        {
            return UnitOfWork.Expenses
                                    .WithAggregate(ExpensesManagerDataModelAggregates.WithExpenseDetails)
                                    .OrderByDescending(c => c.CreatedOn)
                                    .ToList();
        }

        #endregion
    }
}

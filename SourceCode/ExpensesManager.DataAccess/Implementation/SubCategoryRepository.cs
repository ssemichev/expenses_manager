﻿using System;
using ExpensesManager.DataAccess.DataModel;
using ExpensesManager.DataAccess.Interfaces;
using Mindscape.LightSpeed;

namespace ExpensesManager.DataAccess.Implementation
{
    public class SubCategoryRepository : WebApplicationRepository<SubCategory, Int32>, ISubCategoryRepository
    {
        public SubCategoryRepository(LightSpeedContext<ExpensesManagerDataModelUnitOfWork> context)
            : base(context)
        {
        }
    }
}

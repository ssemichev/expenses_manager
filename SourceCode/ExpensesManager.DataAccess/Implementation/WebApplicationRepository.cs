﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Linq.Expressions;
using ExpensesManager.Common.Exceptions;
using ExpensesManager.Common.Interfaces;
using ExpensesManager.DataAccess.DataModel;
using ExpensesManager.DataAccess.Interfaces;
using Mindscape.LightSpeed;
using Mindscape.LightSpeed.MetaData;
using Mindscape.LightSpeed.Querying;

namespace ExpensesManager.DataAccess.Implementation
{
    public abstract class WebApplicationRepository<T, TU> : IEntity<T, TU>,
        IRepository<ExpensesManagerDataModelUnitOfWork, LightSpeedContext<ExpensesManagerDataModelUnitOfWork>>
        where T : Entity<TU>
    {
        private readonly PerRequestUnitOfWorkScope<ExpensesManagerDataModelUnitOfWork> _scope;
        private readonly ExpensesManagerDataModelUnitOfWork _unitOfWork;
        private readonly LightSpeedContext<ExpensesManagerDataModelUnitOfWork> _context;

        protected WebApplicationRepository(LightSpeedContext<ExpensesManagerDataModelUnitOfWork> context)
        {
            _context = context;
            _scope = new PerRequestUnitOfWorkScope<ExpensesManagerDataModelUnitOfWork>(_context);

            //Select ExpensesManagerDataModelUnitOfWork or PerRequestUnitOfWorkScope<ExpensesManagerDataModelUnitOfWork>
            if (_scope == null || !_scope.HasCurrent)
            {
                _unitOfWork = _context.CreateUnitOfWork();
                if (_scope != null)
                {
                    _scope.Dispose();
                }
            }
            else
            {
                _unitOfWork = _scope.Current;
            }
        }

        #region Implementation of IEntity<T, TU>

        /// <summary>
        /// Retrieves business entity
        /// </summary>       
        /// <param name="entityId">The entity id</param>
        /// <returns>The business entity</returns>
        public virtual T GetEntity(TU entityId)
        {
            return UnitOfWork.FindById<T>(entityId);
        }

        /// <summary>
        /// Returns all existing entities
        /// </summary>
        public virtual IEnumerable<T> GetAll()
        {
            return UnitOfWork.Find<T>().ToList();
        }

        /// <summary>
        /// Searches for enities that match the conditions defined by the specified predicate
        /// </summary>
        public virtual IEnumerable<T> FindBy(Expression<Func<T, bool>> predicate)
        {
            if (predicate == null) throw new ArgumentNullException("predicate");

            throw new NotImplementedException("FindBy");
        }

        /// <summary>
        /// Searches for enities that match the conditions defined by the specified predicate
        /// </summary>
        /// <example>repository.FindBy(Entity.Attribute("Id") == criteria.EntityId).ToList();</example>
        public virtual IEnumerable<T> FindBy(QueryExpression predicate)
        {
            if (predicate == null) throw new ArgumentNullException("predicate");
            return UnitOfWork.Find<T>(predicate);
        }

        /// <summary>
        /// Insert a new entity
        /// </summary>
        /// <param name="entity">The business entity</param>
        /// <returns>The business entity</returns>
        public virtual T CreateEntity(T entity)
        {
            UnitOfWork.Add(entity);
            return entity;
        }

        /// <summary>
        /// Update value fields of an existing entity
        /// </summary>
        /// <param name="entity">The business entity</param>
        /// <returns>The business entity</returns>
        public virtual T UpdateEntity(T entity)
        {
            if (entity == null || (entity.EntityState != EntityState.New && entity.EntityState != EntityState.Modified)) return entity;
            var originalEntity = GetEntity(entity.Id);

            if (originalEntity == null)
            {
                throw new ExpensesManagerException(String.Format("Cannot update unexisting {0} entity. EntityId: {1}", entity.GetType(), entity.Id));
            }

            var originalEntityInfo = originalEntity.EntityInfo();

            foreach (var field in entity.EntityInfo().ValueFields.Where(f => !f.IsIdentityField && !f.IsManagedField && !f.IsReadOnly && f.IsDefined(entity)))
            {
                var field1 = field;
                originalEntityInfo.ValueFields.First(f => f.FieldName == field1.FieldName).SetValue(originalEntity, field.GetValue(entity));
            }

            return originalEntity;
        }

        /// <summary>
        /// Delete an existing entity
        /// </summary>
        /// <param name="entityId">The entity id</param>
        public virtual void DeleteEntity(TU entityId)
        {
            var entity = UnitOfWork.FindById<T>(entityId);
            if (entity != null)
            {
                UnitOfWork.Remove(entity);
                UnitOfWork.SaveChanges();
                UnitOfWork.Detach(entity);
            }
        }

        /// <summary>
        /// Saves the entity
        /// </summary>
        public virtual void Save()
        {
            UnitOfWork.SaveChanges();
        }

        #endregion

        #region Implementation of IRepository

        public ExpensesManagerDataModelUnitOfWork UnitOfWork
        {
            get { return _unitOfWork; }
        }

        public LightSpeedContext<ExpensesManagerDataModelUnitOfWork> Context
        {
            get { return _context; }
        }

        /// <summary>
        /// Begins a transaction against the underlying database.
        /// </summary>
        /// <returns>The System.Data.IDbTransaction</returns>
        public IDbTransaction BeginTransaction()
        {
            return UnitOfWork.BeginTransaction();
        }

        /// <summary>
        /// Begins a transaction against the underlying database.
        /// </summary>
        /// <param name="isolationLevel">The System.Data.IsolationLevel of the transaction</param>
        /// <returns>An System.Data.IDbTransaction</returns>
        public IDbTransaction BeginTransaction(IsolationLevel isolationLevel)
        {
            return UnitOfWork.BeginTransaction(isolationLevel);
        }

        #endregion

        #region Implementation of IDisposable

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        /// <filterpriority>2</filterpriority>
        public void Dispose()
        {
            if (_scope != null && _scope.HasCurrent)
            {
                _scope.Current.Dispose();
                _scope.Dispose();
            }
            else if (UnitOfWork != null)
            {
                UnitOfWork.Dispose();
            }
        }

        #endregion
    }
}

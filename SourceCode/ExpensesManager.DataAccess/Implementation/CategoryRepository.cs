﻿using System;
using ExpensesManager.DataAccess.DataModel;
using ExpensesManager.DataAccess.Interfaces;
using Mindscape.LightSpeed;

namespace ExpensesManager.DataAccess.Implementation
{
    public class CategoryRepository : WebApplicationRepository<Category, Int32>, ICategoryRepository
    {
        public CategoryRepository(LightSpeedContext<ExpensesManagerDataModelUnitOfWork> context)
            : base(context)
        {
        }
    }
}

﻿using System;
using System.Runtime.Serialization;

namespace ExpensesManager.Common.Exceptions
{
    /// <summary>
    /// Common exception type for all application layers.
    /// Used to distinguish from platform-level errors.
    /// </summary>
    [Serializable]
    public class ExpensesManagerException : ApplicationException
    {
        /// <summary>
        /// Standard ctr.
        /// </summary>
        public ExpensesManagerException() { }
        /// <summary>
        /// Standard ctr.
        /// </summary>
        public ExpensesManagerException(string message) : base(message) { }
        /// <summary>
        /// Standard ctr.
        /// </summary>
        public ExpensesManagerException(string message, Exception inner) : base(message, inner) { }
        /// <summary>
        /// Standard ctr.
        /// </summary>
        public ExpensesManagerException(SerializationInfo info, StreamingContext context) : base(info, context) { }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace ExpensesManager.Common
{
    public static class TranslationExtensions
    {
        /// <summary>
        /// Translates list of Int32to array of objects
        /// </summary>
        /// <param name="list">The list of integers.</param>
        /// <returns>The array of objects.</returns>
        public static object[] ToArrayOfObjects(this IList<Int32> list)
        {
            if (list == null || list.Count == 0) return new object[0];

            var tagids = new object[list.Count];
            list.ToArray().CopyTo(tagids, 0);

            return tagids;
        }

        /// <summary>
        /// Translates list of Int32 to array of objects
        /// </summary>
        /// <param name="list">The list of integers.</param>
        /// <returns>The array of objects.</returns>
        public static object[] ToArrayOfObjects(this IList<Int32?> list)
        {
            if (list == null || list.Count == 0) return new object[0];

            var tagids = new object[list.Count];
            list.ToArray().CopyTo(tagids, 0);

            return tagids;
        }

        public static object[] ToArrayOfObjects<T>(this IList<T> list)
        {
            if (list == null || list.Count == 0) return new object[0];

            var tagids = new object[list.Count];
            list.ToArray().CopyTo(tagids, 0);

            return tagids;
        }
    }
}

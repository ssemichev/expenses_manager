﻿using System;
using System.Collections;

namespace ExpensesManager.Common.IoC
{
    /// <summary>
    /// Implements Service Locator Pattern
    /// </summary>
    public interface IServiceLocator
    {
        Object Resolve(Type serviceType);
        Object Resolve(Type serviceType, string serviceName);

        /// <summary>
        /// Tries to resolve the component, but return null instead of throwing if it is not there.
        /// Useful for optional dependencies.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        T TryResolve<T>();

        /// <summary>
        /// Tries to resolve the compoennt, but return the default value if could not find it, instead of throwing.
        /// Useful for optional dependencies.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="defaultValue">The default value.</param>
        /// <returns></returns>
        T TryResolve<T>(T defaultValue);
        T Resolve<T>();
        T Resolve<T>(String name);
        T Resolve<T>(object argumentsAsAnonymousType);
        T Resolve<T>(IDictionary parameters);
        Boolean IsInitialized { get; }
        Array ResolveAll(Type service);
        T[] ResolveAll<T>();
    }
}

﻿using System;
using System.Collections;
using Castle.Windsor;

namespace ExpensesManager.Common.IoC
{
    public class WindsorServiceLocator : IServiceLocator, IDisposable
    {
        #region Implementation of IServiceLocator

        public object Resolve(Type serviceType)
        {
            return Current.Resolve(serviceType);
        }

        public object Resolve(Type serviceType, string serviceName)
        {
            return Current.Resolve(serviceName, serviceType);
        }

        /// <summary>
        /// Tries to resolve the component, but return null instead of throwing if it is not there.
        /// Useful for optional dependencies.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public T TryResolve<T>()
        {
            return TryResolve(default(T));
        }

        /// <summary>
        /// Tries to resolve the compoennt, but return the default value if could not find it, instead of throwing.
        /// Useful for optional dependencies.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="defaultValue">The default value.</param>
        /// <returns></returns>
        public T TryResolve<T>(T defaultValue)
        {
            return Current.Kernel.HasComponent(typeof(T)) == false ? defaultValue : Current.Resolve<T>();
        }

        public T Resolve<T>()
        {
            return Current.Resolve<T>();
        }

        public T Resolve<T>(string name)
        {
            return Current.Resolve<T>(name);
        }

        public T Resolve<T>(object argumentsAsAnonymousType)
        {
            return Current.Resolve<T>(argumentsAsAnonymousType);
        }

        public T Resolve<T>(IDictionary parameters)
        {
            return Current.Resolve<T>(parameters);
        }

        public bool IsInitialized
        {
            get { return _container != null; }
        }

        public Array ResolveAll(Type service)
        {
            return Current.ResolveAll(service);
        }

        public T[] ResolveAll<T>()
        {
            return Current.ResolveAll<T>();
        }

        #endregion

        public void Initialize(IWindsorContainer windsorContainer)
        {
            if (_container == null) { _container = windsorContainer; }
        }

        public void Reset()
        {
            if (_container != null) { _container.Dispose(); }
        }

        private static IWindsorContainer Current
        {
            get
            {
                var result = _container;
                if (result == null) throw new InvalidOperationException("The container has not been initialized! Please call IoC.Initialize(container) before using it.");
                return result;
            }
        }

        #region Implementation of IDisposable

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            Reset();
        }

        #endregion

        #region Private Members

        private static IWindsorContainer _container;

        #endregion
    }
}
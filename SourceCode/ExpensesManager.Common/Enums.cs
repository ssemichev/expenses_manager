﻿namespace ExpensesManager.Common
{
    public enum MethodInvocation { BeginInvoke, EndInvoke, Exception };
}

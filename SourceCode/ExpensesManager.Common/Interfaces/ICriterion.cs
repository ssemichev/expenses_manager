﻿using System;

namespace ExpensesManager.Common.Interfaces
{
    public interface ICriterion
    {
        /// <summary>
        /// The number of results per page.
        /// </summary>
        Int32? ResultsPerPage { get; set; }
        /// <summary>
        /// Index of the row, zero-based.
        /// </summary>
        Int32? RowIndex { get; set; }
        /// <summary>
        /// Sort expression.
        /// </summary>
        String SortExpression { get; set; }
    }
}
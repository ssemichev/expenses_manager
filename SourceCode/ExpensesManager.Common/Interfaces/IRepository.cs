﻿using System.Data;

namespace ExpensesManager.Common.Interfaces
{
    public interface IRepository<out TK, out TU>
        where TK : class, new()
        where TU : class, new()
    {
        TK UnitOfWork { get; }
        TU Context { get; }

        /// <summary>
        /// Begins a transaction against the underlying database.
        /// </summary>
        /// <returns>The System.Data.IDbTransaction</returns>
        IDbTransaction BeginTransaction();

        /// <summary>
        /// Begins a transaction against the underlying database.
        /// </summary>
        /// <param name="isolationLevel">The System.Data.IsolationLevel of the transaction</param>
        /// <returns>An System.Data.IDbTransaction</returns>
        IDbTransaction BeginTransaction(IsolationLevel isolationLevel);

        void Save();
    }
}

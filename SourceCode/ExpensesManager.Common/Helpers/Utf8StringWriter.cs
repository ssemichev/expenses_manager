﻿using System.IO;
using System.Text;

namespace ExpensesManager.Common.Helpers
{
    public class Utf8StringWriter : StringWriter
    {
        public override Encoding Encoding
        {
            get { return Encoding.UTF8; }
        }
    }
}

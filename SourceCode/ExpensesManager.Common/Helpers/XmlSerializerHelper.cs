﻿using System;
using System.IO;
using System.Runtime.Serialization;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using ExpensesManager.Common.Exceptions;

namespace ExpensesManager.Common.Helpers
{
    public class XmlSerializerHelper
    {
        /// <summary>
        ///  Uses DataContractSerializer to serialize the object.
        /// </summary>
        public static String ToXml<T>(T item)
        {
            return ToXml(item, typeof(T));
        }

        public static T FromXml<T>(String xml)
        {
            if (String.IsNullOrEmpty(xml)) return default(T);

            var formatter = new DataContractSerializer(typeof(T));

            using (var stringReader = new StringReader(xml))
            {
                using (var reader = new XmlTextReader(stringReader))
                {
                    try
                    {
                        return (T)formatter.ReadObject(reader);
                    }
                    catch (SerializationException ex)
                    {
                        throw new ExpensesManagerException("Could not deserialize the object", ex);
                    }
                }
            }
        }

        public static String ToXml(Object item, Type itemType)
        {

            using (TextWriter writer = new Utf8StringWriter())
            {
                using (var xmlWriter = XmlWriter.Create(writer, new XmlWriterSettings { Indent = true }))
                {
                    new DataContractSerializer(itemType)
                        .WriteObject(xmlWriter, item);
                }
                return writer.ToString();
            }
        }

        /// <summary>
        /// Serilaize the object into XML fragment (no XML declaration nor namespace references in the root element). 
        /// </summary>
        /// <typeparam name="T">The type of the object that this System.Xml.Serialization.XmlSerializer can serialize.</typeparam>
        /// <param name="item">The object to serialize.</param>
        /// <returns>XML fragment without XML declaration and namespace references in the root element.</returns>
        public static String ToXmlFragment<T>(T item)
        {
            return ToXmlFragment(item, typeof(T));
        }

        /// <summary>
        /// Serilaize the object into XML fragment (no XML declaration nor namespace references in the root element). 
        /// </summary>
        /// <param name="item">The object to serialize.</param>
        /// <param name="itemType">>The type of the object that this System.Xml.Serialization.XmlSerializer can serialize.</param>
        /// <returns>XML fragment without XML declaration and namespace references in the root element.</returns>
        public static String ToXmlFragment(Object item, Type itemType)
        {
            var serializer = new XmlSerializer(itemType);

            var output = new StringBuilder();
            var emptyNamespace = new XmlSerializerNamespaces();
            emptyNamespace.Add(String.Empty, String.Empty);

            using (var writer = XmlWriter.Create(output, new XmlWriterSettings { OmitXmlDeclaration = true }))
            {
                try
                {
                    serializer.Serialize(writer, item, emptyNamespace);
                }
                catch (Exception ex)
                {
                    throw new ExpensesManagerException("Could not serialize the object", ex);
                }
            }

            return output.ToString();
        }

        /// <summary>
        ///  Deserilaize the object from XML fragment (no XML declaration nor namespace references in the root element).
        /// </summary>
        /// <typeparam name="T">The type of the object to deserialize.</typeparam>
        /// <param name="xml">XML fragment to deserialize.</param>
        /// <returns>The deserialized object.</returns>
        public static T FromXmlFragment<T>(String xml)
        {
            if (String.IsNullOrEmpty(xml)) return default(T);

            var serializer = new XmlSerializer(typeof(T));

            using (var reader = new StringReader(xml))
            {
                try
                {
                    return (T)serializer.Deserialize(reader);
                }
                catch (Exception ex)
                {
                    throw new ExpensesManagerException("Could not deserialize the object", ex);
                }
            }
        }
    }
}

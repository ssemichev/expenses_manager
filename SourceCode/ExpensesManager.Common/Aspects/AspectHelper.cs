﻿using System;
using System.Linq;
using System.Text;
using Castle.Core.Logging;
using Castle.DynamicProxy;
using ExpensesManager.Common.Helpers;

namespace ExpensesManager.Common.Aspects
{
    public class AspectHelper
    {
        public static String CreateInvocationLogString(IInvocation invocation, ILoggerFactory loggerFactory, MethodInvocation methodInvocation)
        {
            var sb = new StringBuilder();

            try
            {
                if (methodInvocation == MethodInvocation.Exception)
                {
                    sb.AppendFormat("Exception during {0}.{1} call. (", invocation.TargetType.Name, invocation.Method.Name);
                }
                else
                {
                    sb.AppendFormat("{0}.{1}.{2}(", invocation.TargetType.Name, invocation.Method.Name, methodInvocation);
                }

                switch (methodInvocation)
                {
                    case MethodInvocation.BeginInvoke:
                        foreach (var argument in invocation.Arguments)
                        {
                            var argumentDescription = argument == null ? "null" : DumpObject(argument);
                            sb.Append(argumentDescription).Append(",");
                        }
                        if (invocation.Arguments.Any()) sb.Length--;
                        sb.Append(")");
                        break;
                    case MethodInvocation.EndInvoke:
                        sb.AppendFormat(") Return value: {0})", invocation.ReturnValue == null ? "null" : DumpObject(invocation.ReturnValue));
                        break;
                    default:
                        foreach (var argument in invocation.Arguments)
                        {
                            var argumentDescription = argument == null ? "null" : argument.ToString();
                            sb.Append(argumentDescription).Append(",");
                        }
                        if (invocation.Arguments.Any()) sb.Length--;
                        sb.Append(")");
                        break;
                }
            }
            catch (Exception exception)
            {
                //Suppress internal exception
                loggerFactory.Create(invocation.TargetType).Error("Unexpected exception occurred.", exception);
            }

            return sb.ToString();
        }

        private static string DumpObject(Object argument)
        {
            if (argument == null) return "";

            var objType = argument.GetType();

            if (objType == typeof(String) || objType.IsPrimitive || !objType.IsClass ||
                (objType.FullName != null && (objType.FullName.StartsWith("ExpensesManager.DataAccess") || objType.FullName.StartsWith("Mindscape.LightSpeed")
                || objType.FullName.StartsWith("System.Collections.Generic."))))
            {
                return argument.ToString();
            }

            return XmlSerializerHelper.ToXmlFragment(argument, objType);
        }
    }
}

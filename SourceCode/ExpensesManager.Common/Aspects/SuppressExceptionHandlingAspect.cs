﻿using System;
using Castle.Core.Logging;
using Castle.DynamicProxy;

namespace ExpensesManager.Common.Aspects
{
    public class SuppressExceptionHandlingAspect : IInterceptor
    {
        public SuppressExceptionHandlingAspect(ILoggerFactory loggerFactory)
        {
            _loggerFactory = loggerFactory;
        }

        public void Intercept(IInvocation invocation)
        {
            try
            {
                invocation.Proceed();
            }
            catch (Exception exception)
            {
                _loggerFactory.Create(invocation.TargetType).Error(AspectHelper.CreateInvocationLogString(invocation, _loggerFactory, MethodInvocation.Exception), exception);
            }
        }

        #region Private Members

        private readonly ILoggerFactory _loggerFactory;

        #endregion
    }
}

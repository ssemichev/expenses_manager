﻿using System;
using System.Collections.Generic;
using System.Linq;
using Castle.Core.Logging;
using Castle.DynamicProxy;

namespace ExpensesManager.Common.Aspects
{
    public class LoggingAspect : IInterceptor
    {
        public LoggingAspect(IExtendedLoggerFactory loggerFactory)
        {
            LoggerFactory = loggerFactory;
            LoggerLevel = LoggerLevel.Debug;
        }

        public void Intercept(IInvocation invocation)
        {
            var hasMethod = HasMethod(invocation.Method.Name);

            if (hasMethod)
            {
                try
                {
                    PreInvoke(invocation);
                }
                catch (Exception exception)
                {
                    //Suppress internal aspect exception
                    LoggerFactory.Create(invocation.TargetType).Error("Unexpected exception occurred.", exception);
                }
            }

            invocation.Proceed();

            if (hasMethod)
            {
                try
                {
                    PostInvoke(invocation);
                }
                catch (Exception exception)
                {
                    //Suppress internal aspect exception
                    LoggerFactory.Create(invocation.TargetType).Error("Unexpected exception occurred.", exception);
                }
            }
        }

        public virtual void PreInvoke(IInvocation invocation)
        {
            var message = AspectHelper.CreateInvocationLogString(invocation, LoggerFactory, MethodInvocation.BeginInvoke);

            switch (LoggerLevel)
            {
                case LoggerLevel.Info: LoggerFactory.Create(invocation.TargetType).Info(message); break;
                case LoggerLevel.Warn: LoggerFactory.Create(invocation.TargetType).Warn(message); break;
                case LoggerLevel.Error: LoggerFactory.Create(invocation.TargetType).Error(message); break;
                case LoggerLevel.Fatal: LoggerFactory.Create(invocation.TargetType).Fatal(message); break;
                default: LoggerFactory.Create(invocation.TargetType).Debug(message); break;
            }
        }

        public virtual void PostInvoke(IInvocation invocation)
        {
            var message = String.Format("{0}.{1}.EndInvoke. Return value: ({2})", invocation.TargetType.Name, invocation.Method.Name,
                invocation.ReturnValue == null ? "null" : 
                        (invocation.ReturnValue.ToString().Length < 500) ? invocation.ReturnValue.ToString() : invocation.ReturnValue.ToString().Substring(0,490));

            switch (LoggerLevel)
            {
                case LoggerLevel.Info: LoggerFactory.Create(invocation.TargetType).Info(message); break;
                case LoggerLevel.Warn: LoggerFactory.Create(invocation.TargetType).Warn(message); break;
                case LoggerLevel.Error: LoggerFactory.Create(invocation.TargetType).Error(message); break;
                case LoggerLevel.Fatal: LoggerFactory.Create(invocation.TargetType).Fatal(message); break;
                default: LoggerFactory.Create(invocation.TargetType).Debug(message); break;
            }
        }

        public IList<String> MethodsNames { get; set; }

        public LoggerLevel LoggerLevel { get; set; }

        private Boolean HasMethod(String methodName)
        {
            //Applies for all method if the list of Methodnames is empty
            if (MethodsNames == null || MethodsNames.Count == 0) return true;

            return MethodsNames.Any(methodsName => methodsName.Equals(methodName));
        }

        #region Protected Members

        protected readonly IExtendedLoggerFactory LoggerFactory;
        protected IExtendedLogger Logger = NullLogger.Instance;

        #endregion
    }
}

﻿using System;
using System.Web;
using Castle.Core.Logging;
using Castle.DynamicProxy;
using Castle.MicroKernel;
using ExpensesManager.Common.Exceptions;

namespace ExpensesManager.Common.Aspects
{
    public class ExceptionHandlingAspect : IInterceptor
    {
        public ExceptionHandlingAspect(ILoggerFactory loggerFactory, IKernel container)
        {
            _loggerFactory = loggerFactory;
            _container = container;
        }

        public void Intercept(IInvocation invocation)
        {
            try
            {
                invocation.Proceed();
            }
            catch (ExpensesManagerException exception)
            {
                _loggerFactory.Create(invocation.TargetType).Error(AspectHelper.CreateInvocationLogString(invocation, _loggerFactory, MethodInvocation.Exception), exception);
                throw;
            }
            catch (Exception exception)
            {
                _loggerFactory.Create(invocation.TargetType).Error(AspectHelper.CreateInvocationLogString(invocation, _loggerFactory, MethodInvocation.Exception), exception);

                var context = _container.Resolve<HttpContextBase>();
                throw new ExpensesManagerException((context.Session != null) ? String.Format("Unexpected exception occurred. ExceptionId: {0}", context.Session.SessionID) : "");
            }
        }

        #region Private Members

        private readonly ILoggerFactory _loggerFactory;
        private readonly IKernel _container;

        #endregion
    }
}
